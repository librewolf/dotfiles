set rnu nu
filetype plugin indent on 
set tabstop=2
set shiftwidth=2
set expandtab 
set backspace=indent,eol,start 
set autoindent 
set ruler 
set incsearch

set splitbelow splitright
noremap <C-up> :vertical resize +3<CR>
noremap <C-down> :vertical resize -3<CR>
