alias ls="ls --color=auto"
alias du="du -h --max-depth=1 | sort -hr"
alias e="exit"
alias ci3="vim /home/adam/.config/i3/config"
export PS1="\u:\w\\$ \[$(tput sgr0)\]"
export EDITOR="vim"

xset r rate 300 50
